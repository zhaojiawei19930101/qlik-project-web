import React, { Component } from 'react';
import Routes from './routes';
import { browserHistory } from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class App extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <div>
          <Routes history={browserHistory} />
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
