import React from 'react';
import { Router, Route } from 'react-router';
import { browserHistory } from 'react-router';
import Messages from './messages';
import NewMessage from './new_message';
const Routes = (props) => (
	<Router {...props}>
        	<Route path='/messages' component={Messages} />
		<Route path='/new_message' component={NewMessage} />
	</Router>
);

export default Routes;