import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import axios from 'axios';

export default class Message extends Component {
    constructor(props){
        super(props);
        this.state = {
            deleteDialogOpen: false,
            moreDialogOpen: false,
            currentDetails: null
        }
        this._onDelete = this._onDelete.bind(this)
        this._onMore = this._onMore.bind(this)
    }
    _onDelete(){
        this.setState({
            deleteDialogOpen: false
        })
        axios.delete('https://qlik-project.herokuapp.com/messages/'+ this.props.messageId).then((response)=>{
            window.location.reload()
        }).catch((error)=>{
        });
    }
    _onMore(){
        axios.get('https://qlik-project.herokuapp.com/messages/'+ this.props.messageId).then((response)=>{
            this.setState({
                currentDetails: response.data.message,
                moreDialogOpen: true
            })
        }).catch((error)=>{
            
        });
    }
    render() {
        const dialogActions = [
        <FlatButton
            label="Cancel"
            primary={true}
            onClick={()=>{
                this.setState({
                    deleteDialogOpen: false
                })
            }}
        />,
        <FlatButton
            label="Delete"
            primary={true}
            onClick={this._onDelete}
        />,
        ];
        return (
            <div style = {styles.container}>
                <div style = {styles.body}>
                    <div style = {styles.content}>   
                        {this.props.content}
                    </div>
                </div>
                <div style = {styles.actionBar}>
                    <div style = {styles.userName}>
                        {"By: " + this.props.userName}
                    </div>
                    <div style = {{marginRight: '15px'}}>
                        <FlatButton label="DELETE" onClick={()=>{
                            this.setState({
                                deleteDialogOpen: true
                             })
                        }}/>
                        <FlatButton label="MORE" onClick={this._onMore}/>
                    </div>
                </div>
                <Dialog
                    title="Are you sure to delete?"
                    actions={dialogActions}
                    modal={true}
                    open={this.state.deleteDialogOpen}
                >
                </Dialog>
                <Dialog
                    title="More Information"
                    modal={false}
                    onRequestClose={()=>{
                        this.setState({
                            moreDialogOpen: false
                        })
                    }}
                    open={this.state.moreDialogOpen}
                >
                {(()=>{
                    if(this.state.currentDetails != null){
                        var date = new Date(this.state.currentDetails.created_at)
                        var dateStrings = date.toString().split(' ')
                        dateStrings.splice(dateStrings.length - 1,1);
                        dateStrings.splice(dateStrings.length - 1,1);
                        return (<div>
                            { "Submitted at: " + dateStrings.join(' ') }
                            <p>{this.state.currentDetails.is_palindrome ? "This is a Palindrome" : "This is not a Palindrome"}</p>
                        </div>)
                    }
                })()}
                </Dialog>
            </div>
        )
    }
}
const styles = {
    container:{
		display:'flex',
		flexDirection: "column",
        justifyContent: "start",
        marginTop:'36px',
        border: '1px solid #C8C8CC'
    },
    body:{
        display:'flex',
		flexDirection: "column",
        justifyContent: "start"
    },
    content:{
        marginTop:'24px',
        marginBottom:'24px',
        fontFamily: 'Avenir-Medium',
        fontSize: '16px',
        color: '#1D1D26',
        width:'93%',
        alignSelf:'center'
    },
    actionBar:{
        display:'flex',
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems:'center',
        background: '#D8D8D8',
        fontFamily: 'Avenir-Medium',
        fontSize: '16px',
        color: '#1D1D26',
        height: '45px'
    },
    userName:{
        marginLeft: '36px'
    }
}
