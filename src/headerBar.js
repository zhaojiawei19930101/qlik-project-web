import React, { Component } from 'react';

export default class HeaderBar extends Component {
    render() {
        return (
            <div style = {styles.headerBar}>
                {this.props.headerText}
            </div>
        )
    }
}
const styles = {
    headerBar:{
        display:'flex',
		flexDirection: "row",
		justifyContent: "center",
        alignItems: "center",
        background: '#FFFFFF',
        boxShadow: '2px 2px 4px 0 rgba(0,0,0,0.50)',
        height: '88px',
        fontFamily: 'Avenir-Heavy',
        fontSize: '36px',
        color: '#000000'
    }
}