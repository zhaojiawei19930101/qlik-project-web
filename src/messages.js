import React, { Component } from 'react';
import axios from 'axios';
import HeaderBar from './headerBar';
import CircularProgress from 'material-ui/CircularProgress';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Message from './message'
export default class Messages extends Component {
    constructor(props){
        super(props);
        this.state = {
            messages: null
        }
    }

    componentDidMount(){
        axios.get('https://qlik-project.herokuapp.com/messages').then((response)=>{
             this.setState({
                 messages: response.data.messages
             })
        }).catch((error)=>{
        });
    }
    render() {

        return (
            !this.state.messages ? (<div style={styles.container}><CircularProgress style={{marginTop:200, alignSelf:'center'}} size={60} thickness={5} /></div>) : (
                <div style = {styles.container}>
                    <HeaderBar headerText="Messages"/>
                    {(()=>{
                        var messages = []
                        for(var i = 0;i < this.state.messages.length;i++){
                            messages.push(<Message messageId={this.state.messages[i].id} key={i} content={this.state.messages[i].content} userName={this.state.messages[i].user_name}/>);
                        }
                        return messages
                    })()}
                </div>
            )           
        )
    }

    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    getChildContext() {
        return {
            muiTheme: getMuiTheme()
        }
    }
}
const styles = {
    container:{
		display:'flex',
		flexDirection: "column",
        justifyContent: "start"
    }
    
}
