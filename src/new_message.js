import React, { Component } from 'react';
import axios from 'axios';
import HeaderBar from './headerBar';
import CircularProgress from 'material-ui/CircularProgress';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { browserHistory } from 'react-router';

export default class NewMessage extends Component {
    constructor(props){
        super(props);
        this.state = {
            userName: '',
            content: ''
        }
        this._userNameChange = this._userNameChange.bind(this)
        this._contentChange = this._contentChange.bind(this)
        this._onSubmit = this._onSubmit.bind(this)
    }
    _userNameChange(e){
        this.setState({
            userName: e.target.value
        })
    }
    _contentChange(e){
        this.setState({
            content: e.target.value
        })
    }
    _onSubmit(){
        if((this.state.userName.trim().length==0 || this.state.userName.trim().length>255) || this.state.content.trim().length==0){

            return
        }
        var data = {
            user_name:this.state.userName.trim(),
            content:this.state.content
        }
        axios.post('https://qlik-project.herokuapp.com/messages',{message: data}).then((response)=>{
            console.log(response)
            browserHistory.push('/messages');
        }).catch((error)=>{
        });
    }
    
    render() {

        return (
                <div style = {styles.container}>
                    <HeaderBar headerText="Submit A New Message"/>
                    <TextField
                        value={this.state.userName}
                        floatingLabelText="Your Name *"
                        onChange={this._userNameChange}
                        style={{alignSelf:'center', width:'30%'}}
                    />
                    <TextField
                        value={this.state.content}
                        floatingLabelText=" Message Content *"
                        multiLine={true}
                        rows={10}
                        rowsMax={10}
                        onChange={this._contentChange}
                        style={{alignSelf:'center', width:'30%'}}
                        textareaStyle={{
                            border: '1px solid #C8C8CC'
                        }}
                        underlineShow={false}
                    />
                    <RaisedButton label="Submit" primary={true} style={{alignSelf:'center',width:'30%'}} onClick={this._onSubmit} />
                    </div>        
        )
    }

    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    getChildContext() {
        return {
            muiTheme: getMuiTheme()
        }
    }
}
const styles = {
    container:{
		display:'flex',
		flexDirection: "column",
        justifyContent: "start"
    }
}
